/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "prestwood-internal.h"
#include <glib.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

static gboolean gPrestwoodInitialized = FALSE;
static Prestwood *gPrestwood = NULL;
static void unref_data (gpointer data, gpointer userdata);
static void add_mount_and_unref_list_data (gpointer data, gpointer userdata);
static void prestwood_init_volume_monitor (Prestwood *media);
static void shut_volume_monitor (Prestwood *media);
static void prestwood_send_mount_changed (Prestwood *media, GFile *mountFile, gboolean added);
static void prestwood_add_mount (Prestwood *media, gchar *mountName, GFile *mountFile);
static void prestwood_vmon_mount_added_cb (GVolumeMonitor *volumeMonitor, GMount *mount, gpointer userdata);
static gint gfile_compare (gconstpointer file1, gconstpointer file2);
static void prestwood_delete_mount (gpointer data, gpointer userdata);
static void prestwood_remove_mount (Prestwood *media, gchar *mountName, GFile *mountFile, gboolean deleteLink);
static void prestwood_vmon_mount_removed_cb (GVolumeMonitor *volumeMonitor, GMount *mount, gpointer userdata);
//static guint prestwood_get_num_mounts (Prestwood *prestwood);
static GList *prestwood_get_mount_list (Prestwood *prestwood);
static gint gfile_compare_uri (gconstpointer data, gconstpointer userdata);
static gboolean prestwood_path_is_mounted (Prestwood *prestwood, gchar *path);
static GList *prestwood_get_tracker_local_media_list (void);
static GList *prestwood_get_local_media_list (Prestwood *prestwood);
static gboolean init_volume_monitor(gpointer data);

static void on_bus_acquired (GDBusConnection *connection,
		const gchar     *name,
		gpointer         user_data);
static void on_name_acquired (GDBusConnection *connection,
		const gchar     *name,
		gpointer         user_data);

static void on_name_lost (GDBusConnection *connection,
		const gchar     *name,
		gpointer         user_data);

gint prestwood_init (void)
{
	if (gPrestwoodInitialized)
		return 0;

	if (gPrestwood)
	{
		// Shutdown all monitors
		shut_volume_monitor (gPrestwood);
		g_free (gPrestwood);
		gPrestwood = NULL;
	}

	gPrestwood = g_new0 (Prestwood, 1);

	// Allocate the internal structures
	gPrestwood->volume = g_new0 (PrestwoodVolume, 1);
	gPrestwood->media = g_new0 (PrestwoodLocalMedia, 1);

	/* all services are required to own a bus before they export their interfaces */
	gPrestwood->owner_id = g_bus_own_name ( G_BUS_TYPE_SESSION,           // bus type
			"org.apertis.Prestwood",       // interface name
			G_BUS_NAME_OWNER_FLAGS_NONE,  // bus own flag, can be used to take away the bus and give it to another service
			on_bus_acquired,              // callback invoked when the bus is acquired
			on_name_acquired,             // callback invoked when interface name is acquired
			on_name_lost,                 // callback invoked when name is lost to another service or other reason
			NULL,                         // user data
			NULL);                        // user data free func

	// Volume monitor
	g_timeout_add(300,( GSourceFunc ) init_volume_monitor, NULL );
	gint watch;

	// Initialization done !
	gPrestwoodInitialized = TRUE;

	return 0;
}

static gboolean init_volume_monitor(gpointer data)
{
	prestwood_init_volume_monitor (gPrestwood);
	return FALSE;
}

static void on_bus_acquired (GDBusConnection *connection,
		const gchar     *name,
		gpointer         user_data)
{
	DEBUG ("with name: %s", name);

	/* here we export all the methods supported by the service, this callback is called before on_name_acquired*/
	/* once on_name_acquired is called clients can start calling service interfaces, so its important to export all methods here */

	/* Creates a new skeleton object, ready to be exported */
	Prestwood *prestwood=prestwood_get_default();
	prestwood->prestwood_object = prestwood_service_skeleton_new ();

	/* provide signal handlers for all methods */
	g_signal_connect (prestwood->prestwood_object,
			"handle-mount-list",
			G_CALLBACK (handle_mount_list),
			user_data);

	g_signal_connect (prestwood->prestwood_object,
			"handle-is-mounted",
			G_CALLBACK (handle_is_mounted),
			user_data);

	/* default property value setting*/
	//prestwood_service_set_num_mounts(prestwood->prestwood_object,2);

	g_signal_connect (prestwood->prestwood_object,
			"handle-local-media",
			G_CALLBACK (handle_local_media),
			user_data);

	if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (prestwood->prestwood_object),
				connection,
				"/org/apertis/Prestwood/Service",
				NULL))
	{
      	 DEBUG ("interface export error ");
	}

}

static void on_name_acquired (GDBusConnection *connection,
		const gchar     *name,
		gpointer         user_data)
{
	DEBUG ("with name: %s ", name);
}

static void on_name_lost (GDBusConnection *connection,
		const gchar     *name,
		gpointer         user_data)
{
	/* free all dbus handlers ... */
	DEBUG ("entered ");
}

Prestwood *prestwood_get_default (void)
{
	return gPrestwood;
}

void prestwood_shutdown (void)
{
	if (gPrestwood)
	{
		// Shutdown all monitors
		shut_volume_monitor (gPrestwood);
		g_free (gPrestwood);
		gPrestwood = NULL;
	}

	// Shutdown done !
	gPrestwoodInitialized = FALSE;
}

static void unref_data (gpointer data, gpointer userdata)
{
	g_object_unref (data);
}

static void add_mount_and_unref_list_data (gpointer data, gpointer userdata)
{
	if (userdata == NULL)
		return;

	Prestwood *media = (Prestwood *) userdata;
	if (media->volume == NULL)
		return;

	GFile *mountFile = g_mount_get_root (data);
	gchar *mountName = g_mount_get_name (data);
	if (mountName != NULL && mountFile != NULL)
		prestwood_add_mount (media, mountName, mountFile);

	unref_data (data, NULL);
}

static void prestwood_volume_mounted_cb (GVolume      *volume,
		GAsyncResult *res,
		gpointer      userdata)
{
	GMount *mount;
	GError *error = NULL;

	/* FIXME: Do proper error handling here. */
	g_volume_mount_finish (volume, res, &error);
	Prestwood *media = userdata;

	if (error != NULL) 
	 {
	   WARNING ("%s: code %d: %s", g_quark_to_string (error->domain),
           	    error->code, error->message);
	   g_error_free (error);
	 }

	mount = g_volume_get_mount (volume);

	/* mount is NULL if the operation failed. */
	if (mount == NULL)
		return;

	gchar *mountName = g_mount_get_name (mount);
	GFile *mountFile = g_mount_get_root (mount);

	// Add mount and send app-mgr notification
	prestwood_add_mount (media, mountName, mountFile);

	// Send notification to all registered clients
	prestwood_send_mount_changed (media, mountFile, TRUE);

	g_object_unref (mount);
	g_object_unref (mountFile);
	g_free (mountName);
}

static void prestwood_volume_added_cb (GVolumeMonitor *volume_monitor,
		GVolume        *volume,
		gpointer        user_data)
{
	gchar *volume_name;

	volume_name = g_volume_get_name (volume);

	DEBUG ("Mounting volume, '%s'", volume_name);

	if (!g_volume_can_mount (volume)) 
	 {
	   DEBUG ("The volume '%s' inserted cannot be mounted", volume_name);
	 }

	if (!g_volume_should_automount (volume))
	 {
	   DEBUG ("The volume '%s' inserted will not be automounted ", volume_name);
	 }
	//else // currently not working
	{
	  DEBUG ("Mount the volume '%s' manually ", volume_name);
	  g_volume_mount (volume,
	  	          G_MOUNT_MOUNT_NONE,
			  NULL,
			  NULL,
			  (GAsyncReadyCallback) prestwood_volume_mounted_cb,
			  user_data);
	}
}

static void prestwood_volume_removed_cb (GVolumeMonitor *volume_monitor,
		GVolume        *volume,
		gpointer        user_data)
{
	gchar* volume_name = g_volume_get_name (volume);
	DEBUG ("Unmounting volume, '%s'", volume_name);
	GMount *mount;
	mount = g_volume_get_mount (volume);

	if (mount == NULL)
		return;

	gchar *mountName = g_mount_get_name (mount);

	GFile *mountFile = g_mount_get_root (mount);

	// Remove mount and send app-mgr notification
	prestwood_remove_mount (user_data, mountName, mountFile, TRUE);

	// Send notification to all registered clients
	prestwood_send_mount_changed (user_data, mountFile, FALSE);

	g_volume_eject_with_operation(  volume,
			G_MOUNT_UNMOUNT_NONE,
			NULL ,
			NULL ,
			NULL ,
			NULL );

}

static void prestwood_init_volume_monitor (Prestwood *media)
{
	if (media == NULL)
		return;

	// Get currently mounted drives
	media->volume->volumeMonitor = g_volume_monitor_get ();
	if (media->volume->volumeMonitor == NULL)
		return;

	media->volume->mounts = NULL;

	GList *mountedMedia = NULL;
	mountedMedia = g_volume_monitor_get_mounts (media->volume->volumeMonitor);

	// Build a GFile list of all mounts
	if (mountedMedia != NULL)
	{
		g_list_foreach (mountedMedia, add_mount_and_unref_list_data, media);
		g_list_free (mountedMedia);
	}


	g_signal_connect ( media->volume->volumeMonitor, "volume-added",
			G_CALLBACK (prestwood_volume_added_cb), media);
	// g_signal_connect ( media->volume->volumeMonitor, "volume-removed",
	//                    G_CALLBACK (prestwood_volume_removed_cb), media);

	// Connect to the volume monitor signals
	//g_signal_connect (media->volume->volumeMonitor, "mount-added",
	//		G_CALLBACK (prestwood_vmon_mount_added_cb), media);
	g_signal_connect (media->volume->volumeMonitor, "mount-pre-unmount",
			G_CALLBACK (prestwood_vmon_mount_removed_cb), media);
	g_signal_connect (media->volume->volumeMonitor, "mount-removed",
			G_CALLBACK (prestwood_vmon_mount_removed_cb), media);

}

static void shut_volume_monitor (Prestwood *media)
{

	g_signal_handlers_disconnect_by_func (media->volume->volumeMonitor, prestwood_volume_added_cb, media);
	g_signal_handlers_disconnect_by_func (media->volume->volumeMonitor, prestwood_volume_removed_cb, media);
	g_signal_handlers_disconnect_by_func (media->volume->volumeMonitor, prestwood_vmon_mount_added_cb, media);
	g_signal_handlers_disconnect_by_func (media->volume->volumeMonitor, prestwood_vmon_mount_removed_cb, media);
	// Not sure if an extra is needed !
	g_signal_handlers_disconnect_by_func (media->volume->volumeMonitor, prestwood_vmon_mount_removed_cb, media);

	if (media->volume->mounts == NULL)
		return;

	g_list_foreach (media->volume->mounts, prestwood_delete_mount, media);
	g_list_free (media->volume->mounts);
	media->volume->mounts = NULL;

	unref_data (media->volume->volumeMonitor, NULL);
	media->volume->volumeMonitor = NULL;
}

static void prestwood_send_mount_changed (Prestwood *media, GFile *mountFile, gboolean added)
{
	if (media == NULL || mountFile == NULL)
		return;

	gchar *uri = g_file_get_uri (mountFile);
	if (uri == NULL)
		return;

	prestwood_service_emit_mount_changed (
			media->prestwood_object,
			"client-name",
			added,
			(const gchar *)uri);

}


static void prestwood_add_mount (Prestwood *media, gchar *mountName, GFile *mountFile)
{
	if (media == NULL || mountName == NULL || mountFile == NULL)
		return;

	gchar *uri = g_file_get_uri (mountFile);
	if (uri == NULL)
		return;

	if (media->volume == NULL)
		return;
	GCancellable *cancellable=NULL;
	media->volume->mounts = g_list_append (media->volume->mounts, mountFile);
	DEBUG (" prestwood_add_mount");
	gint length=g_list_length(media->volume->mounts);
	if(media->prestwood_object)
		prestwood_service_set_num_mounts(media->prestwood_object,length);
	DEBUG (" prestwood_add_mount  length is %i ",
        prestwood_service_get_num_mounts (gPrestwood->prestwood_object));
}

static void prestwood_vmon_mount_added_cb (GVolumeMonitor *volumeMonitor, GMount *mount, gpointer userdata)
{
	if (userdata == NULL)
		return;

	Prestwood *media = userdata;

	gchar *mountName = g_mount_get_name (mount);
	GFile *mountFile = g_mount_get_root (mount);

	// Add mount and send app-mgr notification
	prestwood_add_mount (media, mountName, mountFile);
	g_free (mountName);
	//GList *c = g_list_first (media->clientsForMountChanged);

	// Send notification to all registered clients
	prestwood_send_mount_changed (media, mountFile, TRUE);
}

static gint gfile_compare (gconstpointer file1, gconstpointer file2)
{
	if (g_file_equal ((GFile *) file1, (GFile *) file2))
		return 0;

	return -1;
}

static void prestwood_delete_mount (gpointer data, gpointer userdata)
{
	if (data == NULL || userdata == NULL)
		return;

	Prestwood *media = userdata;
	if (media->volume == NULL || media->volume->mounts == NULL)
		return;

	GFile *rmMountFile = data;
	// Get the mount name
	GMount *mount = g_file_find_enclosing_mount (rmMountFile, NULL, NULL);
	gchar *mountName = NULL;
	if (mount != NULL)
		mountName = g_mount_get_name (mount);

	prestwood_remove_mount (media, mountName, rmMountFile, FALSE);

	// Send notification to all registered clients
	//GList *c = g_list_first (media->clientsForMountChanged);
	DEBUG (" prestwood_delete_mount ");

	prestwood_send_mount_changed (media, rmMountFile, FALSE);

	if (mount != NULL)
		g_object_unref (mount);
	if (mountName != NULL)
		g_free (mountName);
}

static void prestwood_remove_mount (Prestwood *media, gchar *mountName, GFile *mountFile, gboolean deleteLink)
{
	if (media == NULL || mountFile == NULL)
		return;

	gchar *uri = g_file_get_uri (mountFile);
	if (uri == NULL)
		return;

	if (media->volume == NULL)
		return;
	GCancellable *cancellable=NULL;

	GList *rmMount = g_list_find_custom (media->volume->mounts, mountFile, gfile_compare);
	DEBUG (" prestwood_remove_mount  uri %s ", uri);
	if (rmMount != NULL)
	{
		DEBUG (" prestwood_remove_mount %s ", mountName);
		if (G_IS_OBJECT (rmMount->data))
			g_object_unref (rmMount->data);

		//if (deleteLink)
		media->volume->mounts = g_list_delete_link (media->volume->mounts, rmMount);
		int length=g_list_length(media->volume->mounts);
		if(prestwood_get_default()->prestwood_object)
			prestwood_service_set_num_mounts(prestwood_get_default()->prestwood_object,length);
		DEBUG (" prestwood_remove_mount %d ", length);
	}
}

static void prestwood_vmon_mount_removed_cb (GVolumeMonitor *volumeMonitor, GMount *mount, gpointer userdata)
{
	Prestwood *media = userdata;

	gchar *mountName = g_mount_get_name (mount);
	GFile *mountFile = g_mount_get_root (mount);
	DEBUG ("prestwood_vmon_mount_removed_cb %s", mountName);
	// Remove mount and send app-mgr notification
	prestwood_remove_mount (media, mountName, mountFile, TRUE);

	// Send notification to all registered clients
	prestwood_send_mount_changed (media, mountFile, FALSE);
}

static GList *prestwood_get_mount_list (Prestwood *prestwood)
{
	if (prestwood == NULL || prestwood->volume == NULL || prestwood->volume->mounts == NULL)
		return NULL;

	if (g_list_length (prestwood->volume->mounts) == 0)
		return NULL;

	GList *mountPaths = NULL;
	GList *m = g_list_first (prestwood->volume->mounts);
	gchar *pFileUri  = NULL ;
	while ((m != NULL)&&(m->data !=NULL))
	{
		pFileUri =   g_file_get_uri (m->data) ;
		if(NULL != pFileUri)
			mountPaths = g_list_append (mountPaths,pFileUri );
		m = g_list_next (m);
	}
	return mountPaths;
}

static gint gfile_compare_uri (gconstpointer data, gconstpointer userdata)
{
	return g_strcmp0 (g_file_get_uri ((GFile *) data), g_file_get_uri ((GFile *) userdata));
}

static gboolean prestwood_path_is_mounted (Prestwood *prestwood, gchar *path)
{
	if (prestwood == NULL || prestwood->volume == NULL || prestwood->volume->mounts == NULL)
		return FALSE;

	if (path == NULL)
		return FALSE;

	if (g_list_length (prestwood->volume->mounts) == 0)
		return FALSE;

	GFile *gFile = g_file_new_for_commandline_arg (path);
	GList *mount = g_list_find_custom (prestwood->volume->mounts, gFile, gfile_compare_uri);
	g_object_unref (gFile);

	if (mount != NULL)
		return TRUE;

	return FALSE;
}

static GList *prestwood_get_tracker_local_media_list (void)
{
	// TODO Get it from tracker-config
	GList *localList = NULL;
	gchar *mediaPath = g_strdup_printf ("file://%s/media", g_get_home_dir ());
	localList = g_list_append (localList, mediaPath);

	return localList;
}

static GList *prestwood_get_local_media_list (Prestwood *prestwood)
{
	if (prestwood == NULL || prestwood->volume == NULL)
		return NULL;

	if (prestwood->media->localMedia == NULL)
		prestwood->media->localMedia = prestwood_get_tracker_local_media_list ();

	return prestwood->media->localMedia;
}

static gint compare_clientinfo_appname (gconstpointer data, gconstpointer userdata)
{
	if (data == NULL || userdata == NULL)
		return -1;

	PrestwoodClientInfo *clientInfo = (PrestwoodClientInfo *) data;
	return g_strcmp0 ((gchar *) clientInfo->appName, (gchar *) userdata);
}

// Always return FALSE
gboolean prestwood_unregister_client (gpointer userdata)
{
	if (userdata == NULL)
		return FALSE;

	Prestwood *media = prestwood_get_default ();
	if (media == NULL || media->clientsForMountChanged == NULL)
		return FALSE;

	gchar *clientName = userdata;
	GList *toRemove = g_list_find_custom (media->clientsForMountChanged, clientName, compare_clientinfo_appname);
	if (toRemove == NULL)
		return FALSE;

	PrestwoodClientInfo *clientInfo = toRemove->data;
	if (clientInfo == NULL)
		return FALSE;

	if (clientInfo->appName != NULL)
		g_free (clientInfo->appName);
	g_free (clientInfo);
	media->clientsForMountChanged = g_list_delete_link (media->clientsForMountChanged, toRemove);

	return FALSE;
}



gboolean handle_local_media(
		PrestwoodService *object,
		GDBusMethodInvocation *invocation,gpointer user_data)
{
	int len=0;
	gchar **localMediaList=NULL;
	GList *local_list=prestwood_get_local_media_list(gPrestwood);
	localMediaList=g_new0(gchar* , g_list_length(local_list)+1);
	while(len<g_list_length(local_list))
	{
		localMediaList[len]=g_strdup(g_list_nth_data(local_list,len));

		len++;

	}
	localMediaList[len]=NULL;
	len=0;
	if(localMediaList)
	{
		prestwood_service_complete_local_media(object,invocation,(const gchar * const*)localMediaList);
		while(localMediaList[len]!=NULL)
		{
			g_free(localMediaList[len]);
			localMediaList[len]=NULL;
			len++;
		}
	}

	return TRUE;
}

gboolean handle_is_mounted (
		PrestwoodService *object,
		GDBusMethodInvocation *invocation,
		const gchar *arg_mountPath,gpointer user_data)
{

	gboolean isMounted=prestwood_path_is_mounted(gPrestwood,(gchar *)arg_mountPath);
	prestwood_service_complete_is_mounted(object,invocation,isMounted);
	return TRUE;
}

gboolean handle_mount_list(
		PrestwoodService *object,
		GDBusMethodInvocation *invocation,gpointer user_data)
{
	int len=0;
	gchar **mountList=NULL;
	gchar *def[]={"null",NULL};
	GList *mount_list =prestwood_get_mount_list(gPrestwood);

	mountList=g_new0(gchar* , g_list_length(mount_list)+1);

	while((NULL != mount_list)&&(NULL != mount_list->data))
	{
		mountList[len]=g_strdup(mount_list->data);
		DEBUG (" mountList is %s", mountList[len]);
		mount_list=g_list_next(mount_list);
		len++;

	}

	mountList[len]=NULL;
	len=0;
	if(mountList)
	{
		prestwood_service_complete_mount_list(object,invocation,(const gchar * const*)mountList);
		while(mountList[len]!=NULL)
		{
			g_free(mountList[len]);
			mountList[len]=NULL;
			len++;
		}

	}
	else
		prestwood_service_complete_mount_list(object,invocation,(const gchar * const*)def);

	return TRUE;

}



