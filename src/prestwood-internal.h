/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __PRESTWOOD_INTERNAL_H__
#define __PRESTWOOD_INTERNAL_H__

#include <gio/gio.h>
#include "prestwood.h"

#define DEBUG(format, ...) g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define CRITICAL(format, ...) g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define WARNING(format, ...) g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)

typedef struct _Prestwood Prestwood;
typedef struct _PrestwoodVolume PrestwoodVolume;
typedef struct _PrestwoodLocalMedia PrestwoodLocalMedia;
typedef struct _PrestwoodClientInfo PrestwoodClientInfo;
typedef struct _PrestwoodUpnp PrestwoodUpnp;
typedef struct _PrestwoodUpnpData PrestwoodUpnpData;
typedef struct _PrestwoodLocalMediaData PrestwoodLocalMediaData;
typedef union _PrestwoodData PrestwoodData;

gboolean handle_local_media(
    PrestwoodService *object,
    GDBusMethodInvocation *invocation,gpointer user_data);

gboolean handle_is_mounted (
    PrestwoodService *object,
    GDBusMethodInvocation *invocation,
    const gchar *arg_mountPath,gpointer user_data);

gboolean handle_mount_list(
    PrestwoodService *object,
    GDBusMethodInvocation *invocation,gpointer user_data);


// Main struct to hold all strage manager data
struct _Prestwood {

	PrestwoodVolume *volume;		// Volume manager data
	PrestwoodLocalMedia *media;		// Currently active media paths

	GList *clientsForMountChanged;		// List (PrestwoodClientInfo) of clients registered for mount change

	PrestwoodService *prestwood_object;
	guint owner_id;
};

// Volume manager related data
struct _PrestwoodVolume
{
	GVolumeMonitor *volumeMonitor;		// Volume monitor
	GList *mounts;						// GFiles of currently mounted storage
};

// Media related data
struct _PrestwoodLocalMedia
{
	GList *localMedia;				// GFiles of currently mounted storage
};

// Local media data
struct _PrestwoodLocalMediaData
{
	gchar *path;
};

// Union of all Data
union _PrestwoodData
{
	PrestwoodLocalMediaData *localData;
};

// App info for mount changed callback
struct _PrestwoodClientInfo
{
	gchar *appName;
};

gint prestwood_init (void);
void prestwood_shutdown (void);
gboolean prestwood_unregister_client (gpointer userdata);
Prestwood *prestwood_get_default (void);

#endif
